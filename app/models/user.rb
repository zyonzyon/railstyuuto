class User < ApplicationRecord
  # micropostモデルと関連,ユーザーが破棄された時ユーザーのマイクロポストも破棄,dependent: :destroy
  has_many :microposts, dependent: :destroy
  # UserとRelationshipの関連付け
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy

  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy

  # following配列の元はfollowed idの集合とする
  has_many :following, through: :active_relationships, source: :followed
  # followers配列の元はfollower idの集合とする
  has_many :followers, through: :passive_relationships, source: :follower
  
  # remember_digestに保存するための仮想カラム,activation_tokenはメール認証とユーザーの有効化,reset_tokenはパス再設定
  attr_accessor :remember_token, :activation_token, :reset_token
  # validが通ってsaveする前にdowncase_emailを実行する
  before_save   :downcase_email
  # ユーザーが作られる直前にメソッドを呼び出す,トークンを作ってハッシュ化してUser.activation_digestに保存
  before_create :create_activation_digest

	# validが通ってsaveする前にブロック内の処理を実行する

	validates :name,  presence: true, length: { maximum: 50 }

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true,
			length: { maximum: 255 },
			format: { with: VALID_EMAIL_REGEX },
      #　一意性 ↓大文字小文字は同一とみなす
			uniqueness: true
     has_secure_password
     # 空文字を禁止にして6文字以上の入力を必要とする、編集ページ(editテンプレート)では空文字の場合は更新しない
     validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

     # ログインテスト用のメソッド
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # ランダムなトークンを返す,cookies,アカウントの有効化のリンク,パスワードリセットのリンクに使う
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # 永続セッションのためにユーザーをデータベースに記憶する
  def remember
    # 仮想カラムに平文のトークンを入れる
    self.remember_token = User.new_token
    # 平文のパスをハッシュ化してDBに保存
    self.update_attribute(:remember_digest, User.digest(remember_token))
  end

  # attribute: remember,activation,reset
  def authenticated?(attribute, token)
    # sendメソッドは呼び出すメソッドを切り替えるため
    digest = send("#{attribute}_digest")
    # remember_digestがnilだったらreturn(以下の行を実行しない)
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # ユーザーのログイン情報を破棄する
  def forget
    self.update_attribute(:remember_digest, nil)
  end
  
  # アカウントを有効にする
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # 有効化用のメールを送信する
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  # パスワード再設定の属性を設定する
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # パスワード再設定のメールを送信する
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  # パスワード再設定の期限が切れている場合はtrueを返す,○時間前hours.ago
  def password_reset_expired?
    self.reset_sent_at < 2.hours.ago
  end

  # ユーザーのステータスフィードを返す
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)
  end
  
  # ユーザーをフォローする
  def follow(other_user)
    # <<で配列の最後に追記
    following << other_user
  end

  # ユーザーをフォロー解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしてたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

  private

    # メールアドレスをすべて小文字にする
    def downcase_email
      self.email = email.downcase
    end

    # 有効化トークンとダイジェストを作成および代入する
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end