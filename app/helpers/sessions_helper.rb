module SessionsHelper

  def log_in(user)
    session[:user_id] = user.id
  end

  # 記憶トークンcookieに対応するユーザーを返す
  def current_user
    user_id = session[:user_id]

    # if user_idはセッションがnilか判定
    if user_id
      # @current_user ||= => a = a or User.find_by〜
      @current_user ||= User.find_by(id: user_id)
      # ユーザークッキーの複合化された:user_idとuser_idが一致したらtrue
      elsif (user_id = cookies.signed[:user_id])

      # 例外処理
      # raise

      # 複合化したidがDBにあればuserに入れる
      user = User.find_by(id: cookies.signed[:user_id])
      # 複合化したidからハッシュ化したトークンを調べる,:rememberで調べるため第一引数に入れる
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # 渡されたユーザーがカレントユーザーであればtrueを返す
  def current_user?(user)
    user && user == current_user
  end

  def logged_in?
  	# current_userがあれば(nilじゃなければ)true,ログインしているかどうかは def current_userが一度でもtrueならok
    !current_user.nil?
  end

  def log_out
    # forget(current_user),DBのremember_digestとクッキーをnilにする
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # ユーザーのセッションを永続的にする
  def remember(user)
    # User.rbのremember
    user.remember
    # cookies.permanentのpermanentはcookiesのexpires:(保存期間)を20年にする
    # signedは署名(暗号化),取り出すときは複合化する
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # 永続的セッションを破棄する
  def forget(user)
    # DBのremember_digestをnilにする
    user.forget
    # ユーザーのクッキーも消去
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # 記憶したURL（もしくはデフォルト値）にリダイレクト
  def redirect_back_or(default)
    # :forwarding_url]があったらそのURL,ないならdefaultに行く
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # アクセスしようとしたURLを覚えておく
  def store_location
    # :forwarding_urlに元々いく予定のURLを記憶する,getリクエストの時のみ
    session[:forwarding_url] = request.original_url if request.get?
  end
end
