module ApplicationHelper
    
    # タイトルが設定されていたらpage_title + base_tatile,違う場合はbase_title
      def full_title(page_title = '')
        base_title = "Ruby on Rails Tutorial Sample App"
        if page_title.empty?
          base_title
        else
          page_title + " | " + base_title
        end
      end
      
end
