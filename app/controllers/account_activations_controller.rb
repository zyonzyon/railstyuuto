class AccountActivationsController < ApplicationController
def edit
    user = User.find_by(email: params[:email])
    # nilガードして、既に有効かされていないかチェック(!user.activated?),idのトークンでactivation_digestを認証
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      # アカウントを有効化
      user.activate
      log_in user
      flash[:success] = "ユーザーアカウントの認証に成功しました"
      redirect_to user
    else
      flash[:danger] = "ユーザーアカウントの認証に必要な情報がありません"
      # 失敗したらトップページに戻す
      redirect_to root_url
    end
  end
end
