class ApplicationController < ActionController::Base

	include SessionsHelper

private

    # ユーザーのログインを確認する,ログイン済みユーザーか確認
    def logged_in_user
      # unlessはifの逆,falseなら〇〇する
      unless logged_in?
      	# 元々行きたかったurlを保存
        store_location
        flash[:danger] = "ログインが必要です"
        redirect_to login_url
      end
    end
end