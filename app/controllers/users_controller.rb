class UsersController < ApplicationController
  # 指定したアクション(indexからfollowersアクション)が実行されたら設定したメソッド(logged_in_user)を呼び出す
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy, :following, :followers]
  # ↑のbefore_actionでチェックをしてから出ないとcorrect_userがうまくチェックできないため
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:destroy]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    # debugger
  end

  def new
  	@user = User.new
  end

  def create
  	# 許可された属性のみ書き込み可能にするため(user_params)を入れる
    @user = User.new(user_params)
    
    # valid判定,セーブする前、validが通った時にメーラーのトークン作成済み
    if @user.save
      # 有効化用のメールを送信
      @user.send_activation_email
      #UserMailer.account_activation(@user).deliver_now
      flash[:info] = "メールを送信しました、メールを開きアカウントを有効化してください。"
      redirect_to root_url
    else
      # 失敗したらnewテンプレートを呼ぶ
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  # DELETE /users/:id
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
    end

  def update
    @user = User.find(params[:id])
    # 更新する
    if @user.update(user_params)
        flash[:success] = "プロフィールを変更しました"
        redirect_to @user
      else
        render 'edit'
      end
    end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  # 脆弱性があるため許可するカラムを設定する
  private
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    # beforeフィルタ

    # 正しいユーザーかどうか確認
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    # 管理者かどうか確認,destroyアクションをする前にチェック
    def admin_user
      redirect_to(root_url) unless current_user.admin?
      end
    end