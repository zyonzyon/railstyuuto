class SessionsController < ApplicationController
  def new
  end

 def create
    user = User.find_by(email: params[:session][:email].downcase)

    # find_byでfalseの場合nilが帰ってくるからif文にuserを入れてnilじゃないかを確認する。
    if user && user.authenticate(params[:session][:password])

      # ログインする前にアカウント認証されているかをチェック
      if user.activated?
        # session[:user_id]にuser.idを入れる
    	   log_in user

    # チェックボックスにチェックされていれば "1",
    if params[:session][:remember_me] == '1'
      # remember(user),ユーザーのセッションを永続的にする(ユーザーのクッキーとDBに情報を保存する)
      remember(user)
    else
      # DBのremember_digestをnil,ユーザーのクッキーも消去
      forget(user)
    end

    # URLが設定されてたらそのURLに行く、なければdefaultに行く
    redirect_back_or user
      
    	# userのリンク先はuser_url(@user) => redirect_to("/users/#{@user.id}")と同一
    	# redirect_to user

    else
      message  = "アカウントの認証がされていません"
      message += "、受信したメールからアカウントの認証を行ってください"
      flash[:warning] = message
      redirect_to root_url
    end
      else
      # flash.nowは次のリクエストがある場合は消える、flashはリクエスト2回目まで残る
      flash.now[:danger] = 'メールアドレスかパスワードが間違っています'
      # render 'new'は'sessions#new'テンプレートを読み込む
      render 'new'
    end
  end

  def destroy
    # ログインしてなければ(!current_user.nil?),log_out = session.delete(:user_id),@current_user = nil
    log_out if logged_in?
    redirect_to root_url
  end
end
