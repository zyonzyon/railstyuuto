require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:michael)
    # クッキーとDBに認証設定
    remember(@user)
  end

  test "current_user returns right user when session is nil" do
    assert_equal @user, current_user
    # !session[:user_id].nil?
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do
    # :remember_digestを新しいトークンに書き換える
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    # current_userがnilか
    assert_nil current_user
  end
end