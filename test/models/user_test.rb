require 'test_helper'

class UserTest < ActiveSupport::TestCase

	# setupはオブジェクトに対してのテスト
   def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
  	# assert メソッドは、第1引数がtrueならテストが成功
  	# valid?は@userが存在するかをチェックする、あればtrueになるからassertはtrue
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = "     "
    # assert_notは@user.valid?がfalseかチェックする
    assert_not @user.valid?
  end

test "email validation should reject invalid addresses" do
   invalid_addresses = %w[user1@example.com user@atfoo.org username@example.com
                           foo@barbaz.com foo@barbaz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

 test "email addresses should be unique" do
    # ↓　.dupはオブジェクトのコピーを作成する
    duplicate_user = @user.dup
    # ↓ 複製したオブジェのメアドに元のメアドを大文字にした値を入れる
    #duplicate_user.email = @user.email.upcase
    @user.save
    # !大文字にしたから同一とみなさないからTrue
    assert_not duplicate_user.valid?
  end

  test "password should be present (nonblank)" do
    @user.password = " " * 6
    @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end

   test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    # following?メソッドであるユーザーをまだフォローしていないことを確認
    assert_not michael.following?(archer)
    # followメソッドを使ってそのユーザーをフォロー
    michael.follow(archer)
    # following?メソッドであるユーザーをフォローしたことを確認
    assert michael.following?(archer)
    # followers.include?であるユーザーからフォローされてるか確認
    assert archer.followers.include?(michael)
    # unfollowメソッドでフォロー解除
    michael.unfollow(archer)
    # following?メソッドであるユーザーのフォロー解除したことを確認
    assert_not michael.following?(archer)
  end

  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    # フォローしているユーザーの投稿を確認
    lana.microposts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # 自分自身の投稿を確認
    michael.microposts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    # フォローしていないユーザーの投稿を確認
    archer.microposts.each do |post_unfollowed|
      assert_not michael.feed.include?(post_unfollowed)
    end
  end
end