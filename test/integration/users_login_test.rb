require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

def setup
    # users(:michael)はusers.ymlのmichaelを呼び出す
    @user = users(:michael)
  end

# ログイン失敗時にflashが消えるかのテスト
  # test "login with invalid information" do
    # get login_path
    # assert_template 'sessions/new'
    # post login_path, params: { session: { email: "", password: "" } }
    # assert_template 'sessions/new'
    # assert_not flash.empty?
    # get root_path
    # assert flash.empty?
  # end

test "login with valid email/invalid password" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email:    @user.email,
                                          password: "invalid" } }
    assert_not is_logged_in?
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    get login_path
    post login_path, params: { session: { email:    @user.email,
                                          password: 'password' } }

    # !current_user.nil?
    assert is_logged_in?

    # assert_redirected_to @user,リダイレクト先が正しいかチェック
    assert_redirected_to @user

    # 指定したリダイレクト先か確認
    follow_redirect!
    assert_template 'users/show'

    # count: 0は前のlogin_pathのリンクの数リンクがなくなっているか確認
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path

    # @user = :id
    assert_select "a[href=?]", user_path(@user)

    # destroyアクション
    delete logout_path

    # not !session[:user_id].nil?
    assert_not is_logged_in?

    assert_redirected_to root_url

    # 2回目のログアウト、ログインしてなければ呼び出さない、redirect_to root_urlにリダイレクト
    delete logout_path

    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_empty cookies[:remember_token]
  end

  test "login without remembering" do
    # cookieを保存してログイン
    log_in_as(@user, remember_me: '1')
    delete logout_path
    # cookieを削除してログイン
    log_in_as(@user, remember_me: '0')
    assert_empty cookies[:remember_token]
  end
end