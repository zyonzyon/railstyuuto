require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

# 各リンクが存在するかどうかをチェック、チェック方法はaタグとhref属性を指定して調べる
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    # ↓ count:2はルートへのリンクが2つあるため
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end
end