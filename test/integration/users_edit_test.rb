require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
  	# log_in_as(@user),users(:michael)でログインする
  	log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }

    assert_template 'users/edit'
  end

  test "successful edit with friendly forwarding" do
    # editページにいく
    get edit_user_path(@user)
    # ログインページに戻される,ログインする
    log_in_as(@user)
    # ログインしたら自動でeditページに飛ぶ
    assert_redirected_to edit_user_url(@user)
    # nameとemailをフォームに入れる
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    # flashを出す
    assert_not flash.empty?
    assert_redirected_to @user
    # DBからデータを取り出す
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end
end