Rails.application.routes.draw do
    root 'static_pages#home'
    #help_path → '/help' → static_pages#help → helpビュー表示
  	get  '/help',    to: 'static_pages#help'
  	get  '/about',   to: 'static_pages#about'
  	get  '/contact', to: 'static_pages#contact'
  	get  '/signup',  to: 'users#new'

    #Sessionのルーティングはフルセットにする必要がないため、名前付きのルーティングを記載
    get    '/login',   to: 'sessions#new'
    post   '/login',   to: 'sessions#create'
    delete '/logout',  to: 'sessions#destroy'

    resources :users do
    member do
      get :following, :followers
    end
  end
    # リソースをeditのみにする,GET  /account_activation/トークン/edit edit  edit_account_activation_url(token)
    resources :account_activations, only: [:edit]
    # パスワードリセット用ルーティング
    resources :password_resets,     only: [:new, :create, :edit, :update]
    # micropostsルーティング
    resources :microposts,          only: [:create, :destroy]
    # フォロー,フォロワー
    resources :relationships,       only: [:create, :destroy]
    # resources一覧, rails routesで全てのルーティングを確認可能
    # GET	/users	index	users_path	すべてのユーザーを一覧するページ
	  # GET	/users/1	show	user_path(user)	特定のユーザーを表示するページ
	  # GET	/users/new	new	new_user_path	ユーザーを新規作成するページ（ユーザー登録）
	  # POST	/users	create	users_path	ユーザーを作成するアクション
	  # GET	/users/1/edit	edit	edit_user_path(user)	id=1のユーザーを編集するページ
	  # PATCH	/users/1	update	user_path(user)	ユーザーを更新するアクション
	  # DELETE	/users/1	destroy	user_path(user)	ユーザーを削除するアクション
end
