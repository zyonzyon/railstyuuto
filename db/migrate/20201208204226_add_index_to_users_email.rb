class AddIndexToUsersEmail < ActiveRecord::Migration[6.0]
  def change
  	# DBの一意性
  	add_index :users, :email, unique: true
  end
end
