# 開発環境用のメインのサンプルユーザーを1人作成する
User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

# 開発環境用の追加のユーザーをまとめて生成する(ブロック内を99回実行)
99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

# ユーザーの一部を対象にマイクロポストを生成する
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(word_count: 5)
  users.each { |user| user.microposts.create!(content: content) }
end

# 以下のリレーションシップを作成する
users = User.all
user  = users.first
# ユーザー3からユーザー51までをフォロー
following = users[2..50]
# ユーザー4からユーザー41に最初のユーザーをフォロー
followers = users[3..40]
# フォロー一覧を表示
following.each { |followed| user.follow(followed) }
# フォロワー一覧を表示
followers.each { |follower| follower.follow(user) }