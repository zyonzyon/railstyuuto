def full_title(page_title = "")
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      puts base_title
    else
      puts page_title + " | " + base_title
    end
  end

a1 = "11"

full_title()

full_title(a1)

def string_message(str = '')
   if str.empty?
     "It's an empty string!"
   else
     "The string is nonempty."
   end
 end

string_message("foobar")

puts string_message("")

string_message
